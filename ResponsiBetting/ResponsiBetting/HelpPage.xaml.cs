﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HelpPage : ContentPage
    {
        public HelpPage()
        {
            NavigationPage.SetTitleIconImageSource(this, "logo.png");
            InitializeComponent();
            GetPins();
            
        }

        private async void GetPins()
        {
            try
            {
                string json;
                var position = new Position();
                var client = new HttpClient();
                var response = await client.GetAsync("https://resbet.herokuapp.com/structures/list");
                json = await response.Content.ReadAsStringAsync();
                RootObjectStructures rootObject = new RootObjectStructures();

                if (json != "")
                {
                    rootObject = JsonConvert.DeserializeObject<RootObjectStructures>(json);
                    foreach (Structures o in rootObject.data)
                    {
                        List<Position> postionList = new List<Position>(await (new Geocoder()).GetPositionsForAddressAsync(o.address));
                        if (postionList.Count != 0)
                        {
                            position = postionList.FirstOrDefault<Position>();
                        }

                        Pin pin = new Pin
                        {
                            Label = String.Format("{0} - {1}", o.name, o.phone),
                            Address = o.address,
                            Type = PinType.Generic,
                            Position = position
                        };
                        map.Pins.Add(pin);
                    }
                }
            }
            catch (InvalidCastException e)
            {
                throw e;
            }


        }


        private async void CercaIndirizzo(object sender, EventArgs e)
        {
            List<Position> postionList = new List<Position>(await(new Geocoder()).GetPositionsForAddressAsync(entryCitta.Text)); 
            if (postionList.Count != 0) { 
                var position = postionList.FirstOrDefault<Position>(); 
                MapSpan mapSpan = new MapSpan(position, 0.01, 0.01);
                map.MoveToRegion(mapSpan);
            }
        }

        private void GoToInfoPage(object sender, EventArgs e)
        {
            Navigation.PushAsync(new InfoPage());
        }
    }


    internal class RootObjectStructures
    {
        public string status { get; set; }
        public List<Structures> data { get; set; }
    }

    internal class Structures
    {
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
    }
}