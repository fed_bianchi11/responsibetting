﻿using SkiaChart;
using SkiaChart.Charts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ResponsiBetting
{
    public class Charts
    {
        public string Label { get; set; }
        public Chart<LineChart> Chart { get; set; }
        public Chart<BarChart> BarChart { get; set; }
        public string interval { get; set; }
        public SkiaSharp.SKColor GridColor { get; set; }
        public int GridLines { get; set; }
        public string sumWon { get; set; }
        public string sumSpent { get; set; }
        public string BGChart { get; set; }
        public string BGColor { get; set; }
    }
}
