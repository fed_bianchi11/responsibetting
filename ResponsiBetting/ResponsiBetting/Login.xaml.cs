﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
        }

        private async void LoginClick(object sender, EventArgs e)
        {
			var user = entryUser.Text;
			var password = entryPassword.Text;

			const string Format = "{{\"email\":\"{0}\",\"password\":\"{1}\"}}";

			//string jsonData = @"{""email"":""" + user + @""",""password"":""" + password +@"""}";

			string jsonData = string.Format(Format, user, password);

			var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

			activityIndicator.IsRunning = true;
			gridLogin.Opacity = 0.5f;
			var myHttpClient = new HttpClient();
			var response = await myHttpClient.PostAsync("https://resbet.herokuapp.com/users/login", content);

			string json = await response.Content.ReadAsStringAsync();

			RootObjectUsers rootObject = new RootObjectUsers();
			rootObject = JsonConvert.DeserializeObject<RootObjectUsers>(json);
			if (rootObject.data.Count == 1)
			{
				foreach (UserInfo i in rootObject.data)
				{
					Settings.Default.ID = i.id;
					Settings.Default.MonthlyLimit = i.monthlyLimit;
					Settings.Default.Email = entryUser.Text;
					Settings.Default.SpentThisMonth = await Settings.Default.setSpentThisMonth();
				}
				Settings.Default.isAuth = true;
				Application.Current.MainPage = new MainPage();
			}

			activityIndicator.IsRunning = false;
			gridLogin.Opacity = 1;
			labelErrato.IsVisible = true;
			
        }

        private void GoToForgotPassword(object sender, EventArgs e)
        {
			Application.Current.MainPage = new ForgotPasswordPage();
        }
        private void GoToCreateAccount(object sender, EventArgs e)
        {
			Application.Current.MainPage = new CreateAccountPage();
        }
    }
}