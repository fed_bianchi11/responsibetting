﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateAccountPage : ContentPage
    {
        public CreateAccountPage()
        {
            InitializeComponent();

            valid8.TextColor = Color.Red;
            validn.TextColor = Color.Red;
            validM.TextColor = Color.Red;
            validm.TextColor = Color.Red;
            valids.TextColor = Color.Red;
            coincidono.TextColor = Color.Red;
            coincidono.Text = "Non coincidono";
        }

        private bool verificaRequisitiBool()
        {

            int pwdValid = 1;

            if (!requisiti.IsVisible)
                requisiti.IsVisible = true;
            if (entryPassword.Text.Length > 7)
            {
                valid8.TextColor = Color.Green;
                pwdValid *= 1;
            }
            else
            {
                valid8.TextColor = Color.Red;
                pwdValid *= 0;
            }
            if (Regex.IsMatch(entryPassword.Text, "[a-z]", RegexOptions.ECMAScript))
            {
                validm.TextColor = Color.Green;
                pwdValid *= 1;
            }
            else
            {
                validm.TextColor = Color.Red;
                pwdValid *= 0;
            }
            if (Regex.IsMatch(entryPassword.Text, "[A-Z]", RegexOptions.ECMAScript))
            {
                validM.TextColor = Color.Green;
                pwdValid *= 1;
            }
            else
            {
                validM.TextColor = Color.Red;
                pwdValid *= 0;
            }
            if (Regex.IsMatch(entryPassword.Text, "[0-9]", RegexOptions.ECMAScript))
            {
                validn.TextColor = Color.Green;
                pwdValid *= 1;
            }
            else
            {
                validn.TextColor = Color.Red;
                pwdValid *= 0;
            }
            if (Regex.IsMatch(entryPassword.Text, "[!,@,#,$,%,^,&,*,?,_,~,£,(,)]", RegexOptions.ECMAScript))
            {
                valids.TextColor = Color.Green;
                pwdValid *= 1;
            }
            else
            {
                valids.TextColor = Color.Red;
                pwdValid *= 0;
            }


            if (pwdValid == 1)
            {
                entryConfirmPassword.IsEnabled = true;
                return true;
            }
            else
            {
                entryConfirmPassword.IsEnabled = false;
                return false;
            }
        }

        private void verificaRequisiti(object sender, TextChangedEventArgs e)
        {
            verificaRequisitiBool();
        }

        private void passwordCoincidono(object sender, TextChangedEventArgs e)
        {
            coincidono.IsVisible = true;
            if (entryPassword.Text == entryConfirmPassword.Text)
            {
                coincidono.Text = "Coincidono";
                coincidono.TextColor = Color.Green;
                if (verificaRequisitiBool())
                    button.IsEnabled = true;
                else
                    button.IsEnabled = false;
                requisiti.IsVisible = false;
            }
            else
            {
                coincidono.Text = "Non coincidono";
                coincidono.TextColor = Color.Red;
                button.IsEnabled = false;
                requisiti.IsVisible = false;
            }
        }



        private async void CreaUtente(object sender, EventArgs e)
        {
            if (entryEmail.Text == null)
                return;
            
            var monthlyLimit = "-1";
            var user = entryEmail.Text;
            var password = entryPassword.Text;
            
            if(entryLimit.Text!=null)
                monthlyLimit = entryLimit.Text;

            const string Format =  "{{\"email\":\"{0}\",\"password\":\"{1}\",\"monthlyLimit\":\"{2}\"}}";

            string jsonData = string.Format(Format, user, password, monthlyLimit);


//            string jsonData = @"{""email"":""" + user+ @""",""password"":""" + password + @""",""monthlyLimit"":""" + monthlyLimit + @"""}";

            var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

            activityIndicator.IsRunning = true;
            griglia.Opacity = 0.5f;
            var myHttpClient = new HttpClient();
            var response = await myHttpClient.PostAsync("https://resbet.herokuapp.com/users/new", content);

            string json = await response.Content.ReadAsStringAsync();

            RootObjectUsers rootObject = new RootObjectUsers();
            rootObject = JsonConvert.DeserializeObject<RootObjectUsers>(json);
            if (rootObject.status == "200")
            {
                imgPopup.Source = "ok.png";
                popup.IsVisible = true;
                await Task.WhenAll(
                     imgPopup.FadeTo(1, 600, Easing.Linear),
                     imgPopup.TranslateTo(0, imgPopup.Y, 1000, Easing.CubicInOut));
                Application.Current.MainPage = new Login();
            }
            else
            {
                imgPopup.Source = "no.png";
                popup.IsVisible = true;
                await Task.WhenAll(
                     imgPopup.FadeTo(1, 600, Easing.Linear),
                     imgPopup.TranslateTo(0, imgPopup.Y, 1000, Easing.CubicInOut));
            }
            popup.IsVisible = false;
            griglia.Opacity = 1;
        }

    
        private void GoToLogin(object sender, EventArgs e)
        {
            Application.Current.MainPage = new Login();
        }

        private void chiudiRequisiti(object sender, FocusEventArgs e)
        {
            requisiti.IsVisible = false;
        }
    }
}