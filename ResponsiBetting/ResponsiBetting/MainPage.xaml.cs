﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        private string year=Settings.Default.Year;
        public MainPage()
        {
            InitializeComponent();
        }
        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();

            if (Settings.Default.NewInserted || year != Settings.Default.Year)
            {
                Settings.Default.NewInserted = false;
                year = Settings.Default.Year;
                statsPage.GetCharts();
            }

            if (Settings.Default.ChangedLimit)
                submitPage.setPercentage();
        }
    }
}