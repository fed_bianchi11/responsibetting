﻿using Newtonsoft.Json;
using SkiaChart;
using SkiaChart.Charts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
		private List<string> quotes = new List<string>() { "You can’t control the wind, but you can set your sails", "Gambling makes boys selfish and cruel as well as men","Gambling: The sure way of getting nothing for something", "It's the risk I like about owning a casino. Some days you win, other days you win more"};
		private List<string> authors = new List<string>() { "Unknown", "Thomas Hughes", "Wilson Milzner", "Paul Lyons" };

		public SettingsPage()
        {
            NavigationPage.SetTitleIconImageSource(this, "logo.png");
			ToolbarItems.Add(new ToolbarItem("LogOut", "logout.png", () => { Settings.Default.Destroy(); Application.Current.MainPage = new Login(); }));
			int index = new Random().Next(0, quotes.Count - 1);
			InitializeComponent();
			motivational = quotes.ElementAt(index);
			author = authors.ElementAt(index);
			GetCharts();
        }


		public async void GetCharts()
		{
			Chart = new Chart<BarChart>(await GenerateBarChart());
			email = Settings.Default.Email;
			monthlyLimit = Settings.Default.MonthlyLimit.ToString();
			year = Settings.Default.Year;
			BindingContext = this;
		}

		private async Task<IEnumerable<BarChart>> GenerateBarChart()
		{
			List<string> labels = new List<string>();
			List<float> values = new List<float>();
			values.Add(0);

			labels.Add("");
			var none = new BarChart(labels, values)
			{
				ChartColor = SKColors.Transparent
			};
			labels.RemoveAt(0);
			labels.Add("Perso");
			var lost = new BarChart(labels, await GetYValues('l'))
			{
				ChartColor = SKColors.Red
			};
			labels.RemoveAt(0);
			labels.Add("Vinto");
			var won = new BarChart(labels, await GetYValues('w'))
			{
				ChartColor = SKColors.Green
			};


			return new List<BarChart> {  won, none, lost };
		}


		private async Task<List<float>> GetYValues(char flag)
		{
			List<float> result = new List<float>();

			var client = new HttpClient();
			
			var response = await client.GetAsync(string.Format("https://resbet.herokuapp.com/bets/sumWon/{0}", Settings.Default.ID));

			if (flag == 'l')
				response = await client.GetAsync(string.Format("https://resbet.herokuapp.com/bets/sumSpent/{0}", Settings.Default.ID));

			string json = await response.Content.ReadAsStringAsync();

			RootObjectSums rootObject = new RootObjectSums();
			if (json != "")
				rootObject = JsonConvert.DeserializeObject<RootObjectSums>(json);

			foreach (Sums i in rootObject.data)
			{
				result.Add(i.sum);
				if (flag == 'w') totVinto = i.sum.ToString();
				else totPerso = i.sum.ToString();
			}

			return result;
		}

		private void ModificaAnno(object sender, EventArgs e)
        {
            Settings.Default.Year = entryAnno.Text;
		}
		private async void ModificaLimite(object sender, EventArgs e)
		{
			var monthlyLimit = Int32.Parse(entryMonthlyLimit.Text);
			var user = Settings.Default.ID;

			const string Format = "{{\"user\":\"{0}\",\"monthlyLimit\":{1}}}";

			string jsonData = string.Format(Format, user, monthlyLimit);

			var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

			var myHttpClient = new HttpClient();
			var response = await myHttpClient.PostAsync("https://resbet.herokuapp.com/users/editMonthlyLimit", content);

			string json = await response.Content.ReadAsStringAsync();

			RootObjectUsers rootObject = new RootObjectUsers();
			rootObject = JsonConvert.DeserializeObject<RootObjectUsers>(json);

			if (rootObject.status.Equals("100"))
			{
				await DisplayAlert("RB", "C'è stato un errore nell'inserimento. Riprova più tardi", "ok");
			}
			else
				Settings.Default.SetMonthlyLimit(monthlyLimit);

		}
		private async void VisualizzaStorico(object sender, EventArgs e)
        {
			await Navigation.PushAsync(new BetsList("", "a"));
		}
        private async void Help(object sender, EventArgs e)
        {
			await Navigation.PushAsync(new HelpPage());
		}

		private async void GotoModificaPassword(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new EditPasswordPage());
		}

		public Chart<BarChart> Chart { get; set; }
		public string totVinto { get; set; }
		public string totPerso { get; set; }
		public string email { get; set; }
		public string monthlyLimit { get; set; }
		public string year { get; set; }
		public string motivational { get; set; }
		public string author { get; set; }

    }
}