﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoPage : ContentPage
    {
        public InfoPage()
        {
            NavigationPage.SetTitleIconImageSource(this, "logo.png");
            InitializeComponent();
        }

        private void cosa(object sender, EventArgs e)
        {
            cosaStack.IsVisible = !cosaStack.IsVisible;
        }
        private void sintomi(object sender, EventArgs e)
        {
            sintomiStack.IsVisible = !sintomiStack.IsVisible;
        }
        private void ostacolo(object sender, EventArgs e)
        {
            ostacoloStack.IsVisible = !ostacoloStack.IsVisible;
        }
        private void strutture(object sender, EventArgs e)
        {
            struttureStack.IsVisible = !struttureStack.IsVisible;
        }
    }
}