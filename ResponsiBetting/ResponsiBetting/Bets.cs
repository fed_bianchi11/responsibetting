﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResponsiBetting
{
    public class RootObjectUsers
    {
        public string status { get; set; }
        public List<UserInfo> data { get; set; }
    }

    public class UserInfo
    {
        public int id;
        public string username;
        public string email;
        public float monthlyLimit;
        public string result;
    }

    public class RootObjectSums
    {
        public string status { get; set; }
        public List<Sums> data { get; set; }
    }

    public class Sums
    {
        public float sum { get; set; }
        public float sumSpent { get; set; }
        public float sumWon { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public int betsNumber { get; set; }
    }

    public class RootObjectBets
    {
        public string status { get; set; }
        public List<Bets> data { get; set; }
    }
    public class Bets
    {
        public string date { get; set; }
        public string title { get; set; }
        public float won { get; set; }
        public float spent { get; set; }
    }

}
