﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordPage : ContentPage
    {
        public ForgotPasswordPage()
        {
            InitializeComponent();
        }

        private async void RecuperaPassword(object sender, EventArgs e)
        {

			var user = entryEmail.Text;

			string jsonData = string.Format("{{\"email\":\"{0}\"}}", user);

			var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

			activityIndicator.IsRunning = true;
			griglia.Opacity = 0.5f;
			var myHttpClient = new HttpClient();
			var response = await myHttpClient.PostAsync("https://resbet.herokuapp.com/users/forgotPassword", content);

			string json = await response.Content.ReadAsStringAsync();

			RootObjectUsers rootObject = new RootObjectUsers();
			rootObject = JsonConvert.DeserializeObject<RootObjectUsers>(json);
			if (rootObject.status.Equals("200"))
            {
                labelErrato.IsVisible = true;
                labelErrato.Text = "La nuova password è stata inviata al tuo indirizzo email";
                labelErrato.TextColor = Color.Green;
                activityIndicator.IsRunning = false;
            }
            else
            {
                labelErrato.IsVisible = true;
                labelErrato.Text = "L'email inserita non è registrata";
                labelErrato.TextColor = Color.Red;
                activityIndicator.IsRunning = false;
            }
            griglia.Opacity = 1;

        }

        private void GoToLogin(object sender, EventArgs e)
        {
            Application.Current.MainPage = new Login();
        }
        private void GoToCreateAccount(object sender, EventArgs e)
        {
            Application.Current.MainPage = new CreateAccountPage();
        }
    }
}