﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubmitPage : ContentPage
    {
		List<string> quotes = new List<string>() { "Eat your betting money but don't bet your eating money", "If there's one thing I'm willing to bet on, it's myself", "The best throw of the dice is to throw them away", "In a bet, there is a fool and a thief", "The safest way to double your money is to fold it over once and put it in your pocket" };
		List<string> authors = new List<string>() { "Horse Riding Proverb", "Beyoncé", "Italian Proverb", "Old Proverb", "Kin Hubbard"};

		public SubmitPage()
        {
            InitializeComponent();
			int index = new Random().Next(0, quotes.Count - 1);
			NavigationPage.SetTitleIconImageSource(this, "logo.png");
			motivational = quotes.ElementAt(index);
			author = authors.ElementAt(index);
			getCategories();
			setPercentage(); 
			BindingContext = this;
		}

		List<int> ids = new List<int>();
		public async void getCategories()
        {
			try
			{
				List<string> categories = new List<string>();
				string json;

				var client = new HttpClient();
				var response = await client.GetAsync("https://resbet.herokuapp.com/categories/list");
				json = await response.Content.ReadAsStringAsync();
				RootObjectCategories rootObject = new RootObjectCategories();

				if (json != "")
				{
					rootObject = JsonConvert.DeserializeObject<RootObjectCategories>(json);
					foreach (Categories o in rootObject.data)
					{
						ids.Add(o.id);
						categories.Add(o.title);
					}
				}

				categoryPicker.ItemsSource = categories;
				categoryPicker.SelectedIndex = 0;
				//ids.ElementAt(categoryPicker.SelectedIndex) è l'id
			}
			catch (InvalidCastException e)
			{
				throw e;
			}
		}

		public void setPercentage()
        {
			
			if (Settings.Default.MonthlyLimit == -1)
				labelSpeso.Text = string.Format("Non hai ancora impostato il tuo limite mensile. Hai speso € {0}", Math.Round(Settings.Default.SpentThisMonth,2));
			else
				labelSpeso.Text = string.Format("Hai speso il {0}% del tuo limite mensile.", Math.Round((Settings.Default.SpentThisMonth * 100) / Settings.Default.MonthlyLimit,2));

		}

        private async void SendToDB(object sender, EventArgs e)
        {


			string giocato = entryGiocato.Text;
			string vinto = entryVinto.Text;
			string date = datePicker.Date.ToString("yyyy-MM-dd");
			int id = ids.ElementAt(categoryPicker.SelectedIndex);

            const string Format = "{{\"won\":{0},\"spent\":{1},\"date\":\"{2}\",\"fk_category\":{3},\"fk_user\":{4}}}";

            string jsonData = string.Format(Format, vinto, giocato, date, id.ToString(), Settings.Default.ID.ToString());

			var content = new StringContent(jsonData, Encoding.UTF8, "application/json");


			var myHttpClient = new HttpClient();
			var response = await myHttpClient.PostAsync("https://resbet.herokuapp.com/bets/new", content);

			if (response.IsSuccessStatusCode)
			{
				griglia.Opacity = 0.5f;
				imgPopup.Source = "ok.png";
				popup.IsVisible = true;
				await Task.WhenAll(
					 imgPopup.FadeTo(1, 600, Easing.Linear),
					 imgPopup.TranslateTo(0, imgPopup.Y, 1000, Easing.CubicInOut));
			}
			else
			{
				griglia.Opacity = 0.5f;
				imgPopup.Source = "no.png";
				popup.IsVisible = true; 
				await Task.WhenAll(
					 imgPopup.FadeTo(1, 600, Easing.Linear),
					 imgPopup.TranslateTo(0, imgPopup.Y, 1000, Easing.CubicInOut));
			}
			popup.IsVisible = false;
			Settings.Default.NewInserted = true;
			entryGiocato.Text = "";
			entryVinto.Text = "";
			datePicker.Date = DateTime.Now;
			Settings.Default.SpentThisMonth = await Settings.Default.setSpentThisMonth();
			griglia.Opacity = 1;
			setPercentage();
		}
		public string motivational { get; set; }
		public string author { get; set; }

    }

    internal class RootObjectCategories
	{
		public string status { get; set; }
		public List<Categories> data { get; set; }
	}

	internal class Categories
	{
		public int id { get; set; }
		public string title { get; set; }
	}

}