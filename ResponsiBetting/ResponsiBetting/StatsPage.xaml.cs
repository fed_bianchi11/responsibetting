﻿
using Newtonsoft.Json;
using SkiaChart;
using SkiaChart.Charts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StatsPage : ContentPage
	{
		private List<string> quotes = new List<string>() { "If there weren't luck involved, I would win every time", "When you make a bet, you're saying something", "It's hard to walk away from a winning streak, even harder to leave the table when you're on a losing one","The gambling known as business looks with austere disfavor upon the business known as gambling" };
		private List<string> authors = new List<string>() { "Phil Hellmuth", "Al Alvarez", "Cara Bertoia", "Ambrose Bierce" };
		private const double MIN_SCALE = 1;
		private const double MAX_SCALE = 2.5;
		private const double OVERSHOOT = 0.15;
		private double StartScale;
		private double StartX, StartY;
		public ObservableCollection<Charts> Charts { get; set; }
		public StatsPage()
		{
			InitializeComponent();
			int index = new Random().Next(0, quotes.Count - 1);
			NavigationPage.SetTitleIconImageSource(this, "logo.png");
			motivational = quotes.ElementAt(index);
			author = authors.ElementAt(index);
			GetCharts();
			BindingContext = this;
		}


		/*	private IEnumerable<LineChart> GenerateLineChartsAnnuale()
			{
				var perdite = new LineChart(GetXValues("year").Result, GetXValues("year").Result)
				{
					ChartColor = SKColors.Red,
					ChartName = "Perdite",
					ShowPoints = true,
					PointRadius=8,
					Width=10
				};
				var vincite = new LineChart(GetXValues("year").Result, GetXValues("year").Result)
				{
					ChartColor = SKColors.Green,
					ChartName = "Vincite",
					ShowPoints = true,
					PointRadius = 8,
					Width = 10
				};


				return new List<LineChart> { vincite, perdite };
				//	return new List<LineChart> { linear, random1, linear3, random2 };
			}
		*/

		public async void GetCharts(){
			StatsCarousel.IsVisible = false;
			progressBar.IsVisible = true;
			progressBarLabel.IsVisible = true;
			progressBar.Progress = 0.0;
			progressBarLabel.Text = "";

			Charts = new ObservableCollection<Charts>();

			for (int i = 1; i < 13; i++)
			{
				await progressBar.ProgressTo(i*0.08, 50, Easing.Linear);
				progressBarLabel.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i));
				Charts.Add(
				new Charts
				{
					Chart = new Chart<LineChart>(await GenerateLineChartsMensile(i.ToString())),
					Label = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i)),
					GridColor = SKColor.Parse("#ff9a76"),
					GridLines = 5,
					interval = i.ToString()
				});
			};

			progressBarLabel.Text = "";
			progressBar.IsVisible = false;
			progressBarLabel.IsVisible = false;
			StatsCarousel.ItemsSource = Charts;
			StatsCarousel.Position = DateTime.Now.Month - 1;
			StatsCarousel.IsVisible = true;
		}

		private async Task<IEnumerable<LineChart>> GenerateLineChartsMensile(string month)
		{
			var lost = new LineChart(getXValues(6, month), await GetYValues(month,'l'))
			{
				ChartColor = SKColors.Red,
				ChartName = "Lost",
				ShowPoints = true,
				PointRadius = 8,
				Width = 10
			};
			var won = new LineChart(getXValues(6, month), await GetYValues(month,'w'))
			{
				ChartColor = SKColors.Green,
				ChartName = "Won",
				ShowPoints = true,
				PointRadius = 8,
				Width = 10
			};

			return new List<LineChart> { lost, won };
		}

		private IEnumerable<float> getXValues(int limit, string month)
        {
			float[] monthDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
			float[] result = { 0, 6, 12, 18, 24, monthDays[int.Parse(month)-1] };
			return result;
        }

		private async Task<List<float>> GetYValues(string month, char flag)
		{
			List<float> result = new List<float>();
			
			var client = new HttpClient();
			string api = "";
			var response=new HttpResponseMessage();

			var year = Settings.Default.Year;
			
			if(flag=='w')
				api=string.Format("https://resbet.herokuapp.com/bets/sumWonXmonthDivided/{0}&{1}&{2}", Settings.Default.ID, month, year);
			if(flag=='l')
				api = string.Format("https://resbet.herokuapp.com/bets/sumSpentXmonthDivided/{0}&{1}&{2}", Settings.Default.ID, month, year);

			response = await client.GetAsync(api);

			string json = await response.Content.ReadAsStringAsync();

			RootObjectSums rootObject = new RootObjectSums();
			if (json != "")
				rootObject = JsonConvert.DeserializeObject<RootObjectSums>(json);

			json = "";

			foreach (Sums i in rootObject.data)
				result.Add(i.sum);

			return result;
		}

		public SKColor GridColor { get; set; }
		public float LegendItemSpacing { get; set; }
		public float LabelTextSize { get; set; }

		public async void GetBetsList(object sender, EventArgs eve)
		{
			var button = sender as Button;
			var month = button.BindingContext;
			await Navigation.PushAsync(new BetsList(month.ToString(),""));
		}


		private void OnTapped(object sender, EventArgs e)
		{
			if (Scale > MIN_SCALE)
			{
				this.ScaleTo(MIN_SCALE, 250, Easing.CubicInOut);
				this.TranslateTo(0, 0, 250, Easing.CubicInOut);
			}
			else
			{
				AnchorX = AnchorY = 0.5;
				this.ScaleTo(MAX_SCALE, 250, Easing.CubicInOut);
			}
		}
		private void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType)
			{
				case GestureStatus.Started:
					StartX = (1 - AnchorX) * Width;
					StartY = (1 - AnchorY) * Height;
					break;

				case GestureStatus.Running:
					AnchorX = Clamp(1 - (StartX + e.TotalX) / Width, 0, 1);
					AnchorY = Clamp(1 - (StartY + e.TotalY) / Height, 0, 1);
					break;
			}
		}

		private void OnPinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
		{
			switch (e.Status)
			{
				case GestureStatus.Started:
					StartScale = Scale;
					AnchorX = e.ScaleOrigin.X;
					AnchorY = e.ScaleOrigin.Y;
					break;
				case GestureStatus.Running:
					double current = Scale + (e.Scale - 1) * StartScale;
					Scale = Clamp(current, MIN_SCALE * (1 - OVERSHOOT), MAX_SCALE * (1 + OVERSHOOT));
					break;
				case GestureStatus.Completed:
					if (Scale > MAX_SCALE)
						this.ScaleTo(MAX_SCALE, 250, Easing.SpringOut);
					else if (Scale < MIN_SCALE)
						this.ScaleTo(MIN_SCALE, 250, Easing.SpringOut);
					break;
			}
		}

		private T Clamp<T>(T value, T minimum, T maximum) where T : IComparable
		{
			if (value.CompareTo(minimum) < 0)
				return minimum;
			else if (value.CompareTo(maximum) > 0)
				return maximum;
			else
				return value;
		}

		public string motivational { get; set; }
		public string author { get; set; }
	}

}