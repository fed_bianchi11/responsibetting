﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Settings.Default.isAuthenticated();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
