﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace ResponsiBetting
{
    public class Settings
    {
        public string Year { get; set; }
        public bool NewInserted { get; set; }
        public bool isAuth { get; set; }
        public static Settings Default = new Settings(DateTime.Now.Year);
        public int ID;
        public float MonthlyLimit;
        public float SpentThisMonth;
        public string Email;
        public bool ChangedLimit;

        public Settings(int year)
        {
            Year = year.ToString();
            NewInserted = false;
            isAuth = false;
            ChangedLimit = false;
        }

        public void isAuthenticated()
        {
            if (!isAuth)
                App.Current.MainPage = new Login();
            else
                App.Current.MainPage = new MainPage();
        }

        public async Task<float> setSpentThisMonth()
        {
            var myHttpClient = new HttpClient();

            string api = string.Format("https://resbet.herokuapp.com/bets/sumSpentXmonthXyear/{0}&{1}&{2}", Settings.Default.ID, DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString());

            var response = await myHttpClient.GetAsync(api);

            string json = await response.Content.ReadAsStringAsync();

            RootObjectSums rootObject = new RootObjectSums();
            rootObject = JsonConvert.DeserializeObject<RootObjectSums>(json);
            if (rootObject.data.Count != 0)
            {
                foreach (Sums i in rootObject.data)
                    return i.sum;
            }
            return 0;
        }

        public void SetMonthlyLimit(int limit)
        {
            MonthlyLimit = limit;
            ChangedLimit = true;
        }

        public void Destroy()
        {
            NewInserted = false;
            isAuth = false;
            ID = 0;
            MonthlyLimit= 0;
            SpentThisMonth = 0;
            Email = "";
            ChangedLimit = false;
         }
    }

}
