﻿using Newtonsoft.Json;
using SkiaChart;
using SkiaChart.Charts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ResponsiBetting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BetsList : ContentPage
	{
		public ObservableCollection<Charts> Charts { get; set; }
		public BetsList(string month, string year)
        {
            InitializeComponent();
			NavigationPage.SetTitleIconImageSource(this, "logo.png");
			GetBetsList(month, year);
			
        }

		public async void GetBetsList(string month, string year)
		{
			try
			{	
				float sumSpent = 0;
				float sumWon = 0;

				var client = new HttpClient();
				string json;
				if (year != "" && month=="")
				{
					Charts = new ObservableCollection<Charts>();
					RootObjectSums rootObject = new RootObjectSums();
					if (year == "a")
					{
						this.month = "Storico";
						var response = await client.GetAsync(string.Format("https://resbet.herokuapp.com/bets/sumsGroupByYear/{0}", Settings.Default.ID));
						json = await response.Content.ReadAsStringAsync();
						if (json != "")
						{
							rootObject = JsonConvert.DeserializeObject<RootObjectSums>(json);
						}
						if (rootObject.data.Count != 0)
						{
							foreach (Sums i in rootObject.data)
							{
								GetCharts(i.year, i.sumSpent, i.sumWon, i.betsNumber);
								sumWon += i.sumWon;
								sumSpent += i.sumSpent;
							}
						}
						else
						{
							labelVuota.IsVisible = true;
							labelVuota2.IsVisible = true;
						}
					}
                    else
                    {
						this.month = year;
						string api = string.Format("https://resbet.herokuapp.com/bets/sumsXyearGroupByMonth/{0}&{1}", Settings.Default.ID, year);

						var response = await client.GetAsync(api);

						json = await response.Content.ReadAsStringAsync();
						if (json != "")
						{
							rootObject = JsonConvert.DeserializeObject<RootObjectSums>(json);
						}
						if (rootObject.data.Count != 0)
						{
							foreach (Sums i in rootObject.data)
							{
								GetCharts(i.month, i.sumSpent, i.sumWon, i.betsNumber);
								sumWon += i.sumWon;
								sumSpent += i.sumSpent;
							}
						}
						else
						{
							labelVuota.IsVisible = true;
							labelVuota2.IsVisible = true;
						}
					}
					list.ItemsSource = Charts;
					this.RowHeight = 150.0f;
					this.GridColor = SKColor.Parse("#ff9a76");
				}
				if(month != "")
				{
					string queryYear = Settings.Default.Year;
					if (year != "")
						queryYear = year;

					string api = string.Format("https://resbet.herokuapp.com/bets/betsXmonth/{0}&{1}&{2}", Settings.Default.ID, month, queryYear);
					this.month = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Int32.Parse(month)));
					RootObjectBets rootObject = new RootObjectBets();

					var response = await client.GetAsync(api);
					json = await response.Content.ReadAsStringAsync();
					if (json != "")
					{
						rootObject = JsonConvert.DeserializeObject<RootObjectBets>(json);
					}

					if (rootObject.data.Count != 0)
					{
						foreach (Bets i in rootObject.data)
						{
							sumSpent += i.spent;
							sumWon += i.won;
						}
						list.ItemsSource = rootObject.data;
					}
					else
					{
						labelVuota.IsVisible = true;
						labelVuota2.IsVisible = true;
					}
					this.RowHeight = 50.0f;
				}


				this.sumSpent = sumSpent.ToString();
				this.sumWon = sumWon.ToString();

				BindingContext = this;
			}
			catch (InvalidCastException e)
			{
				throw e;
			}

		}

		public void GetCharts(string intervallo, float sumSpent, float sumWon, int count)
		{
			string interval;
			string bgChart = "#ffc173";
			int maxCount = 12 * 4;
			
			if (bgcolor != "#ecf3f4")
				bgcolor = "#ecf3f4";
			else
				bgcolor = "#c1d7d9";
			
			if (intervallo.Length == 4)
				interval = intervallo;
			else
			{
				maxCount = 4;
				interval = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Int32.Parse(intervallo)));
			}

			if (count > maxCount || sumSpent > sumWon)
				bgChart = "#A52A2A";
			if (count < maxCount && sumSpent < sumWon)
				bgChart = "#808000";
			if (sumSpent > Settings.Default.MonthlyLimit && Settings.Default.MonthlyLimit!=-1)
				bgChart = "#A52A2A";

			Charts.Add(
					new Charts
					{
						BarChart = new Chart<BarChart>(GenerateBarChart(sumSpent, sumWon)),
						GridColor = SKColor.Parse("#ff9a76"),
						interval = interval,
						sumWon = sumWon.ToString(),
						sumSpent = sumSpent.ToString(),
						Label = intervallo + '-' + this.month,
						BGChart = bgChart,
						BGColor = bgcolor
					});
			
		}

		private IEnumerable<BarChart> GenerateBarChart(float sumSpent, float sumWon)
		{
			List<string> labels = new List<string>();
			List<float> values = new List<float>();
			values.Add(0);

			labels.Add("");
			var none = new BarChart(labels, values)
			{
				ChartColor = SKColors.Transparent
			};
			labels.RemoveAt(0);
			values.RemoveAt(0);
			labels.Add("Perso");
			values.Add(sumSpent);
			var lost = new BarChart(labels, values)
			{
				ChartColor = SKColors.Red
			};
			labels.RemoveAt(0);
			values.RemoveAt(0);
			labels.Add("Vinto");
			values.Add(sumWon);
			var won = new BarChart(labels, values)
			{
				ChartColor = SKColors.Green
			};


			return new List<BarChart> { won, none, lost };
		}

		public String month { get; set; }
		public String sumSpent { get; set; }
		public String sumWon { get; set; }
		public Chart<BarChart> Chart { get; set; }
		public float RowHeight { get; set; }
		public SKColor GridColor { get; set; }
		public string bgcolor { get; set; }

		public async void GetBetsList(object sender, EventArgs eve)
		{
			var button = sender as Button;
			string year = button.BindingContext.ToString();
			string[] exploded = year.Split('-');
			if(exploded[0].Length!=4)
				await Navigation.PushAsync(new BetsList(exploded[0], exploded[1]));
			else
				await Navigation.PushAsync(new BetsList("", exploded[0]));
		}
	}
}