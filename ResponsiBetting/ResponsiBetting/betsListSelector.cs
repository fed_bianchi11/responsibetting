﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ResponsiBetting
{
    class betsListSelector : DataTemplateSelector
    {
		public DataTemplate MonthTemplate { get; set; }

		public DataTemplate YearsTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			if (item.GetType() == typeof(Bets))
				return MonthTemplate;
			else
				return YearsTemplate;
		}
	}
}
